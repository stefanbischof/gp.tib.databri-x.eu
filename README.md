# JenPlane Governance Process

Scientific data management is usually guided by a data lifecycle. Nowadays, many funding agencies ask or require data management plans. Research centers and data repositories also adopt some sort of a lifecycle. Many of the current data lifecycles suffer from the constraints they impose on their practitioners (see [the abstract on ICEI2018](https://icei2018.uni-jena.de/wp-content/uploads/2018/08/paper_46.docx) or [the slides on Slideshare](https://www.slideshare.net/javadch/data-lifecycle-is-not-a-cycle-but-a-plane)).


![image](./docs/canvas1.png)
