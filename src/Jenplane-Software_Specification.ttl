#################################################################
###                    Created by Javad Chamanara             ###
#################################################################

@prefix : 		    <https://databri-x.eu/spo-core/> .
@prefix rdf: 		<http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: 	    <http://www.w3.org/2000/01/rdf-schema#> .
@prefix owl: 		<http://www.w3.org/2002/07/owl#> .
@prefix xsd: 		<http://www.w3.org/2001/XMLSchema#> .
@prefix dc: 		<http://purl.org/dc/elements/1.1/> .
@prefix xml: 		<http://www.w3.org/XML/1998/namespace> .
@prefix oboe: 	    <http://ecoinformatics.org/oboe/oboe.1.2/oboe-core.owl#> .
@prefix sosa:       <http://www.w3.org/ns/sosa/> .
@prefix ssn:        <http://www.w3.org/ns/ssn/> .
@prefix wot: 		<http://xmlns.com/wot/0.1/> .
@prefix swrlb: 	    <http://www.w3.org/2003/11/swrlb#> .
@prefix swrl: 	    <http://www.w3.org/2003/11/swrl#> .
@prefix dul:        <http://www.loa-cnr.it/ontologies/DUL.owl#> .
@prefix cc: 		<http://creativecommons.org/ns#> .
@prefix vs: 		<http://www.w3.org/2003/06/sw-vocab-status/ns#> .
@prefix foaf: 	    <http://xmlns.com/foaf/0.1/> .
@prefix owl2xml: 	<http://www.w3.org/2006/12/owl2-xml#> .
@prefix emmo:       <http://stream-ontology.com/matvoc-emmo> .
@prefix skos:       <http://www.w3.org/2004/02/skos/core#> .
@prefix dcat:       <http://www.w3.org/ns/dcat#> .
@prefix vann:		<http://purl.org/vocab/vann/> .
@prefix bibo:		<http://purl.org/ontology/bibo/> .
@prefix schema:	    <http://schema.org/> .
@prefix dcterms:	<http://purl.org/dc/terms/> .
@base 			    <https://databri-x.eu/spo-core/> .

<https://databri-x.eu/spo-core/> rdf:type owl:Ontology ;
    dc:title "Software Specification Ontology: SPO-Core"@en;
    dc:description "The Software Specification Ontology, described using W3C RDF Schema and the Web Ontology Language to satisfy the requirements of JenPlane's governance process in the content of the DataBri-X project."@en ;
    vann:preferredNamespaceUri "https://databri-x.eu/spo-core/" ;
    vann:preferredNamespacePrefix "spo"@en ;
    bibo:status "draft"@en ;
    schema:schemaVersion "0.0.1" ;
    owl:versionInfo "0.0.1" ;
    dc:creator "Javad Chamanara" ;
    dcterms:license "MIT License"@en ;
    dcterms:modified "2023-02-27" ;
    rdfs:seeAlso <https://databri-x.eu> .

#################################################################
###                     Class Definitions                     ###

:Software rdf:type owl:Class; 
    #owl:equivalentClass x:y
    rdfs:comment "A software system"@en ;
    rdfs:label "Software"@IRI-based; 
    rdfs:label "Software"@en .

# Service and Function are identical things here. 
# Possible better to megre them into one concept e.g., Operation

:Operation rdf:type owl:Class; 
    rdfs:comment "A single, unique, and identifiable operation of a software that can be accesseed and executed by an agent (software or human)."@en ;
    rdfs:label "Operation"@IRI-based; 
    rdfs:label "Operation"@en .

:Service rdf:type owl:Class; 
    owl:equivalentClass :Operation ;
    rdfs:comment "A single, unique, and identifiable operation of a software that can be accesseed and executed."@en ;
    rdfs:label "Service"@IRI-based; 
    rdfs:label "Service"@en .

:Function rdf:type owl:Class; 
    owl:equivalentClass :Operation ;
    rdfs:comment "A single, unique, and identifiable function of a software that can be accesseed and executed."@en ;
    rdfs:label "Function"@IRI-based; 
    rdfs:label "Function"@en .

:Data rdf:type owl:Class; 
    rdfs:comment "A simple or compound, structured or unstructured data object that can be passed to or return from operations. Can be speciliazed to spesific data types"@en ;    
    rdfs:label "Data"@IRI-based; 
    rdfs:label "Data"@en .

:ResourceDemand rdf:type owl:Class; 
    rdfs:comment "The amount of a rsoucre that is demanded for an operation"@en ;    
    rdfs:label "ResourceDemand"@IRI-based; 
    rdfs:label "ResourceDemand"@en .


###                 End of Class Definitions                  ###
#################################################################

#################################################################
###               Object Property Definitions                 ###
:hasOperation rdf:type owl:ObjectProperty ;
    ##owl:inverseOf :xxx ;
    rdfs:comment "The hasOperation property yields the operations of a software."@en ;
    rdfs:label "hasOperation"@IRI-based; 
    rdfs:label "has Operation"@en; 
    rdfs:domain :Software;
    rdfs:range :Operation . 

:demands rdf:type owl:ObjectProperty ;
    ##owl:inverseOf :xxx ;
    rdfs:comment "The demands property determines how much/many of a resource is demanded by an operation."@en ;
    rdfs:label "demands"@IRI-based; 
    rdfs:label "demands"@en; 
    rdfs:domain :Operation;
    rdfs:range :ResourceDemand . 


:hasInput rdf:type owl:ObjectProperty ;
    ##owl:inverseOf :xxx ;
    rdfs:comment "The hasInput property yields the input parameters of an operation."@en ;
    rdfs:label "hasInput"@IRI-based; 
    rdfs:label "has Input"@en; 
    rdfs:domain :Operation;
    rdfs:range :Data . 

:hasOutput rdf:type owl:ObjectProperty ;
    ##owl:inverseOf :xxx ;
    rdfs:comment "The hasOutput property yields the output/return type of an operation."@en ;
    rdfs:label "hasOutput"@IRI-based; 
    rdfs:label "has output"@en; 
    rdfs:domain :Operation;
    rdfs:range :Data . 

:hasEndpoint rdf:type owl:ObjectProperty ;
    rdfs:comment "The hasEndpoint property indicates the URI of the operation, either absolute or relative."@en ;
    rdfs:label "hasEndpoint"@IRI-based; 
    rdfs:label "has Endpoint"@en; 
    rdfs:domain :Operation;
    rdfs:range  xsd:string . 

###            End of Object Property Definitions             ###
#################################################################

#################################################################
###                Data Property Definitions                  ###

:hasCostOf rdf:type owl:DatatypeProperty;
    rdfs:comment "The total processing units in FLOPS that the operation consumes per execution on a banchmarked settings."@en ;
    rdfs:domain :Operation ;
    rdfs:range xsd:float .    

:hasDurationOf rdf:type owl:DatatypeProperty;
    rdfs:comment "The total time nano-seconds that the operation takes per execution on a banchmarked settings."@en ;
    rdfs:domain :Operation ;
    rdfs:range xsd:float .    

:hasEnergyOf rdf:type owl:DatatypeProperty;
    rdfs:comment "The total energy in nano-watts that the operation consumes per execution on a banchmarked settings."@en ;
    rdfs:domain :Operation ;
    rdfs:range xsd:float .    

:hasResourceType rdf:type owl:DatatypeProperty;
    rdfs:comment "The type of the resouce: CPU, GPU, MEM, DISK, NETWORK."@en ;
    rdfs:domain :ResourceDemand ;
    rdfs:range xsd:string .    

:hasResourceUnit rdf:type owl:DatatypeProperty;
    rdfs:comment "The unit of measurement of the resouce: FLOPS, Core, MB, MbS."@en ;
    rdfs:domain :ResourceDemand ;
    rdfs:range xsd:string .

:MinimumRequired rdf:type owl:DatatypeProperty;
    rdfs:comment "The minimum amount of the resource type required for the proper functioning of the operation."@en ;
    rdfs:domain :ResourceDemand ;
    rdfs:range xsd:float .

:MaximumRequested rdf:type owl:DatatypeProperty;
    rdfs:comment "The maximum amount of the resource type requested to be allocated to the operation."@en ;
    rdfs:domain :ResourceDemand ;
    rdfs:range xsd:float .


###            End of Data Property Definitions               ###
#################################################################      